// # Algorithmic
// In the language of your choice, write a function which, taking a positive integer n as input, finds all
// sets of numbers that sum up to n.
// For example, n=4, we have:
// 4
// 3,1
// 2,2
// 2,1,1
// 1,1,1,1
// Note that 2,1,1 is same as 1,2,1 or 1,1,2.

let sumRec = function (target, current, start, result, output) {
  if (current === target) {
    output.push(result.slice().reverse());
  }

  for (let i = start; i < target; i++) {
    let temp_sum = current + i;
    if (temp_sum <= target) {
      result.push(i);
      sumRec(target, temp_sum, i, result, output);
      result.pop();
    } else {
      return;
    }
  }
};

let printAll = function (target) {
  let output = [];
  let result = [];
  sumRec(target, 0, 1, result, output);
  output.push([target])
  return output.reverse().join('\n')
}
console.log(printAll(4));




// function findSum(n) {
//   sumOf([], 1, n)
// }
// const sumOf = (arr, i, n) => {
//   for (let j = i; j <= n; j++) {
//     arr.push(j)
//     // recursive
//     sumOf(arr, j, n - j)
//     arr.pop()
//   }
//   if (n === 0) {
//     console.log(arr)
//   }
// }
// findSum(4);
