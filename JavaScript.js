// # JavaScript
// We have a data service returning json data from another server (Server to Server), you get json-encoded
// as:
// [
//   { "username": "ali", "hair_color": "brown", "height": 1.2 },
//   { "username": "marc", "hair_color": "blue", "height": 1.4 },
//   { "username": "joe", "hair_color": "brown", "height": 1.7 },
//   { "username": "zehua", "hair_color": "black", "height": 1.8 }
// ]

// In an effort to reduce transfer size, we want to transfer the data in the following json format instead:
// {
//   "h": ["username", "hair_color", "height"],
//     "d": [
//       ["ali", "brown", 1.2],
//       ["marc", "blue", 1.4],
//       ["joe", "brown", 1.7],
//       ["zehua", "black", 1.8]
//     ]
// }

// Assignments:
// 1) Write the NodeJS function which, taking in a raw data set as first mentioned, will return the improved
// json-encoded string and expose it into standard RESTFul API
// Notes:
// 1) it can be assumed that all records in the initial JSON result set have the same fields, and that
// said fields are indexed in the same order
// 2) make your functions generic (where the field names of the records in the resultset are not known in
// advance)

let json = JSON.parse('{"h":["username","hair_color","height"],"d":[["ali","brown",1.2],["marc","blue",1.4],["joe","brown",1.7],["zehua","black",1.8]]}');
console.log(json);

function name(json) {
  let result = [];
  for (let i in json.d) {
    result.push({ "username": json.d[i][0], "hair_color": json.d[i][1], "height": json.d[i][2] });
  }
  return result
}

console.log(name(json));