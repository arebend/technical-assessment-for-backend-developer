/*
Write a function which, taking in a positive integer n as input, returns an array of all primes lower than n. 
sample expected output: getPrimes(5); ⇒ array(2, 3) getPrimes(10); ⇒ array(2, 3, 5, 7)
 */

// NodeJS

function getPrimes(number) {
  temp = [];

  for (i = 2; i < number; i++) {
    count = 0;
    for (j = 2; j < i; j++) {
      if ((i % j) == 0) {
        count++;
      }
    }
    if (count == 0) {
      temp.push(i);
    }
  }
  return temp;
}

console.log(getPrimes(10));
